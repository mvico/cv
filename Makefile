CV_SPANISH = cv-spanish
CV_ENGLISH = cv-english
# INPUT_LETTER = 

# OUTPUT = "[CV] Matías Daniel Vico (english)"
OUTPUT_ENGLISH = "CV_Matias_Daniel_Vico_english"
OUTPUT_SPANISH = "CV_Matias_Daniel_Vico_espanol"

METADATA = metadata.yaml
ENGINE = lualatex
TEMPLATE = template/eisvogel
# TEMPLATE = ./cv-template.tex
# CSS = new-moderncv.css
CSS = ""
FILTERS = filter/pandoc-svg.py
# LUA-FILTERS = lua/{month-year.lua,smallcaps.lua}
LUA-FILTERS = lua/{month-year.lua,years-of-experience.lua}

# all: html pdf cover_letter
all: pdf cover_letter

html:
	pandoc --standalone --to $@ -o output/$(OUTPUT).$@ -i $(INPUT) --metadata-file=$(METADATA) -c $(CSS)

pdf:
	pandoc --standalone --to $@ -o output/$(OUTPUT_ENGLISH).$@ -i $(CV_ENGLISH).md --metadata-file=$(METADATA) --template $(TEMPLATE) --pdf-engine=$(ENGINE) --filter=$(FILTERS) --lua-filter=$(LUA-FILTERS) -f markdown+emoji
	pandoc --standalone --to $@ -o output/$(OUTPUT_SPANISH).$@ -i $(CV_SPANISH).md --metadata-file=$(METADATA) --template $(TEMPLATE) --pdf-engine=$(ENGINE) --filter=$(FILTERS) --lua-filter=$(LUA-FILTERS) -V lang=es_ES -f markdown+emoji

# $(CV_SPANISH).pdf $(CV_ENGLISH).pdf: % : cv_%.md
# 	pandoc --standalone --to $@ -o output/$(OUTPUT).$@ -i $(INPUT) --metadata-file=$(METADATA) --template $(TEMPLATE) --pdf-engine=$(ENGINE) --filter=$(FILTERS) --lua-filter=$(LUA-FILTERS) -f markdown+emoji

view-pdf: pdf
	xdg-open output/$(OUTPUT).$^

view-html: html
	xdg-open output/$(OUTPUT).$^

cover_letter:
	pandoc --standalone --to pdf -o output/$@.pdf -i $(INPUT) --metadata-file=$(METADATA) --template $(TEMPLATE) --pdf-engine=$(ENGINE) --filter=$(FILTERS)

clean:
	rm -rf output/$(OUTPUT).{html,pdf}


# $(ECHO) $(FOO): % : %.o
# 	$(CC) $(CFLAGS) -o $@ $^
