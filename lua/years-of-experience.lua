return {
  {
    Str = function (elem)
      local pattern = "{{years_since:%d+/%d+}}"
      local match, _ = string.find(elem.text, pattern)
      if match then
        local date = string.gsub(elem.text, "{{years_since:", "")
        local date = string.gsub(date, "}}", "")
        local i, j = string.find(date, "%d+/")
        local month = string.sub(date, i, j-1)
        local i, j = string.find(date, "/%d+")
        local year = string.sub(date, i+1, j)

        local complete_years = os.date("%Y") - year - 1

        -- First (incomplete) year + Complete years inbetween + Last (current) year
        local total_months = (12 - month) + (complete_years) * 12 + os.date("%m")
        local total_years = math.floor(total_months / 12)

        -- return tostring(total_months)
        return tostring(total_years) -- Change to this once the count of months is correct
      end
    end,
  }
}
