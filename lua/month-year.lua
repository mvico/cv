function Meta(m)

  if m.date == nil then -- If the 'date' field on the metadata is not already present get it automatically

    -- Setting default language when not explicitly defined
    language = os.setlocale() -- Saving the system default value as the initial value
    language = m.lang -- Use the metadata.yml file entry if it exists
    language = PANDOC_WRITER_OPTIONS.variables["lang"] -- Use the CLI 'lang' option if present

    -- Changing the 'locale' to match the 'language' variable
    locale = os.setlocale(tostring(language) .. ".UTF-8")

    m.date = os.date("%B, %Y")
    return m
  end
end
