-- Processing the text for conditional content
-- If there is a flag for conditional content it will be discarded

function Header(elem)
  -- print(elem.content[#elem.content].text)
  if elem.content[#elem.content].text == "{{silent=true}}" then
    return pandoc.RawInline ""
  else
    return elem
  end
end

function Para(elem)
  if elem.content[#elem.content].text == "{{silent=true}}" then
    return pandoc.RawInline ""
  else
    return elem
  end
end
