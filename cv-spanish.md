# En búsqueda de
- Physical Design Engineering
- CAD flow/automation development
- Physical Verification

# Información personal
**Nombre**: Matías Daniel Vico

**Nacionalidad**: Argentina ![Argentina][icon-Argentina]\

**Actualmente viviendo en**: España ![Spain][icon-Spain]\

## Información de contacto

![LinkedIn][icon-LinkedIn]\ **LinkedIn**: [www.linkedin.com/in/matiasvico][My LinkedIn]\

![email][icon-email]\ **e-mail**: [matias.daniel.vico@gmail.com][mailto]\


# Experiencia laboral
<!-- Personal Statement Essentials: As for format, you might dedicate one sentence to stating who you are, another to your background and accomplishments and a third to explain why you’re a good fit for the role. -->
Al día de hoy, cuento con más de {{years_since:3/2017}} años de experiencia en **Diseño físico** _(RTL a GDS)_ y **desarrollo de flujo/automatización CAD** _(para herramientas de Cadence y Synopsys)_.

He contribuido en varios takeouts exitosos, con desarrollados en **nodos establecidos**, así como también en **nodos avanzados, sub-micron: 16, 7 y 5 nm**.

Los principales sectores en los que he trabajado son: **ASICs de comunicaciones coherentes** destinados a la comunicación de "Larga Distancia"/"Metropolitana" y entre y/o dentro de "Data Centers". También, en sistemas solución de entrega de energía.

## Posición actual
Sr. **Physical Design Engineer** en Monolithic Power Systems

> Desde 7/2022

### Responsabilidades
<!-- - Diseño físico digital a nivel de bloque, desde: `RTL` hasta: `GDS` para bloques con múltiples instancias, de varios millones de celdas estándar cada uno -->
- Digital block-level physical design, from `RTL` to `GDS`, for **highly physically constrained digital designs**
- Diseño físico digital de top-level de proyectos
- Diseño de Power Grid custom y Análisis de potencia (Power Analysis)
- Custom Clock Tree Synthesis
- STA (Static Time Analysis, inglés para: "Análisis de Tiempo Estático")
- PV (Physical Verification, inglés para: Verificación Física)
- Implementación de ECOs
- Desarrollo en tareas de CAD y flujo de trabajo para el grupo de herramientas de Cadence

## Puestos previos
Sr. **Physical Design Engineer** en Marvell Corporation

> Desde: 4/2021 hasta: 6/2022

Sr. **Physical Design Engineer** en Inphi Corporation

> Desde: 4/2018 hasta: 4/2021

Jr. **Physical Design Engineer** en Inphi Corporation

> Desde: 3/2017 hasta: 4/2018

### Responsibilities 
- Digital block-level physical design, from `RTL` to `GDS`, for **several multi-million-cells block instances**
- Digital chip-top-level physical design support
- Floorplanning for: block & chip-top level (digital-on-top projects)
- Custom Power Grid design and Power Analysis
- Custom Clock-Tree Synthesis for designs with multiple, global clocks (including its `SDC`'s definition) with frequencies as high as `1.2GHz`
- Focus on Low-Power design
- STA (Static Time Analysis)
- PV (Physical Verification)
- CAD / Physical Design Workflow development for Cadence & Synopsys toolsets


\newpage


# Educación

## ![University][icon-University]\ Universidad

### ![Argentina][icon-Argentina]\ UTN - FRVM
- **Título de Master** en Ingeniería en Electrónica por la UTN - FRVM (Universidad Tecnológica Nacional - Facultad Regional Villa María). Villa María, Córdoba, Argentina.

- **Título de Bachillerato** en Ingeniería en Electrónica por la UTN - FRVM.

El desarrollo de mi carrera educativa se enfocó en:

- Manufactura de ASIC
- Diseño de circuitos integrados analógicos y digitales
- MEMS
- DFT / Verificación
- Sistemas embebidos / FPGA

#### Actividades asociadas
- **GECaM** (Grupo de Estudio en Calidad en Mecatrónica): Grupo de investigación y desarrollo para sistemas mixtos mecánicos y eléctricos.

### ![Germany][icon-Deutschland]\ TUI
Programa de intercambio académico internacional. Colaboración entre la universidad [UTN][UTN]\ y el servicio de intercambio académico alemán ([DAAD][DAAD]\).

El programa de intercambio se desarrolló durante el semestre de verano (_Sommerkurs_) en la [TUI (Technische Universität Ilmenau)][TUI].

El objetivo principal de la beca de intercambio fue tomar un semestre usual de la carrera y colaborar en un programa de investigación en curso en la facultad de mecánica y eléctrica de la universidad.

Tema del proyecto de investigación realizado en colaboración:

- **Desarrollo de amplificador de bajo ruido**: Colaboración en el desarrollo de un amplificador de bajo ruido para la banda de 860 MHz (banda 20). Título original:: _"Projekt MUSIK: 806 MHz (band 20) LNA entwurf"_

Algunas de las materias que tomé en el semestre:

- Nano-tecnología 
- Desarrollo CMOS

#### Actividades asociadas

Todas las actividades asociadas realizadas durante el programa de intercambio tuvieron foco en la interacción con la gente, para aprender y abrazar las culturas de la universidad y la Alemana:

- **Voluntario en Café estudiantil**: Trabajé como voluntario en el café "bc-Studentencafé" dentro del Campus de la TUI.
- **Rugby**: Jugué Rugby en el equipo de Rugby de la TUI.


## ![Scholarships][icon-research]\ Becas de estudio

### ![Argentina][icon-Argentina]\ CIN - Becas de Estímulo a las Vocaciones Científicas 2015

Implementación de: "Oscillation Based Test (OBT)" en circuitos analógicos reconfigurables modernos, una técnica usualmente orientada hacia los sistemas embebidos analógicos. Particularmente, trabajando en plataformas PSOC reconfigurables de Cypress, en placas de desarrollo provistas por el fabricante.

La validación de la estrategia propuesta fue también desarrollada, mediante una campaña de inyección de fallas en el modelo implementado.

Temas principales: 

- OBT
- Diseño Analógico
- Implementaciones de tiempo continuo y señal mixta

> Beca de estudio en el UTN - FRVM (Villa María, Córdoba, Argentina).
>
> Desde: 9/2015 hasta: 8/2016 (1 año).

### ![Argentina][icon-Argentina]\ GECaM (Grupo de Estudio en Calidad en Mecatrónica)
Aplicaciones posibles para la FFT (Fast Fourier Transform) en análisis preventivo sobre sistemas en vibración.

Temas principales:

- Métodos matemáticos necesarios para implementar los cálculos de la FFT
- Diseño del software para un dispositivo de medición de vibración ya existente
- Análisis de la información recolectada para intentar modelar el comportamiento de fallas

> Beca de estudio en el UTN - FRVM (Villa María, Córdoba, Argentina).
>
> Desde: 8/2012 hasta: 7/2014 (2 años).



\newpage



# Habilidades principales / Intereses

## Ingeniería Electrónica
- PD
- LEC
- STA
- PV
- Low-power
- CAD
- Spice
- ASIC
- SoC
- FPGA
- Genus
- Innovus
- Tempus
- Fusion Compiler / ICC2
- PrimeTime / PrimeECO
- Calibre
- StarRC

## Lenguajes de programación / Scripting
- Tcl
- Python
- Bash
<!-- - System Verilog  Should learn it more/use it more to put it here-->
<!-- - [C](#c) this makes a internal link-->
- C
- Lua

## Control de versión / Gestión de proyectos
- Git
- GitLab CI pipelines
- YouTrack

## Sistemas Operativos
- Linux
- Windows
- MacOS

## Misceláneas
- R&D
- Trabajo en equipo
- [tmux / Byobu](https://www.udemy.com/course/linux-tmux/)
- [AWK](https://www.udemy.com/course/awk-tutorial/)


\newpage


# Logros

## Cursos

### Diseño Digital
- ![Argentina][icon-Argentina]\ [**Fundación Fulgor (2018)**][Fundación Fulgor]: "Curso en Diseño Digital Avanzado (DDA)".

### Verificación Digital
- [**Udemy (2021)**](https://www.udemy.com/course/soc-verification-systemverilog/): ["SoC Verification using System Verilog".](https://www.udemy.com/certificate/UC-04780e6a-6a86-4c24-9c71-de5e13e5a120/)

<!-- ![Udemy SoC Verification using System Verilog](certificate/udemy-SoC_Verification_using_System_Verilog.jpg){width=400px}\ -->

### Diseño Analógico
- ![Argentina][icon-Argentina]\ [**Fundación Fulgor (2018)**](https://www.linkedin.com/company/fundaci%C3%B3n-fulgor/): "Diseño Analógico en Circuitos Integrados".
- ![Argentina][icon-Argentina]\ **EAMTA**: "Advanced Analog Design 2: 'Synopsys Custom Designer'".

### Micro-Controladores
- ![Argentina][icon-Argentina]\ **UTN - FRVM**: "Programación básica de PIC C".

### Python
- [**Kaggle (2020)**](https://www.kaggle.com/learn/python): ["Python"](https://www.kaggle.com/learn/certification/matasdanielvico/python). **Descripción del Curso**: Learn the most important language for data science. _By [Colin Morris](https://www.kaggle.com/colinmorris), Data Scientist_.

<!-- [ ![Kaggle "Python"](certificate/kaggle-python.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/python) -->

#### Librerías Python

##### Pandas
- [**Kaggle (2021)**](https://www.kaggle.com/learn/pandas): ["Pandas"](https://www.kaggle.com/learn/certification/matasdanielvico/pandas). **Descripción del Curso**: Solve short hands-on challenges to perfect your data manipulation skills. _By [Aleksey Bilogur](https://www.kaggle.com/residentmario), Data Analyst._

<!-- [ ![Kaggle Pandas](certificate/kaggle-pandas.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/pandas) -->

##### `scikit-learn`
- [**Kaggle (2021)**](https://www.kaggle.com/learn/intro-to-machine-learning): ["Intro to Machine Learning"](https://www.kaggle.com/learn/certification/matasdanielvico/intro-to-machine-learning). **Descripción del Curso**: Learn the core ideas in machine learning, and build your first models. _By [Dan Becker](https://www.kaggle.com/dansbecker), Data Scientist._

<!-- [ ![Kaggle Intro to Machine Learning](certificate/kaggle-Intro_to_Machine_Learning.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/intro-to-machine-learning) -->

<!-- ##### Web & DB -->
<!-- - [**Udemy (2020)**](https://www.udemy.com/course/python-sin-fronteras-html-css-mysql/): ["Python sin fronteras: HTML, CSS, Flask y MySQL"](https://www.udemy.com/certificate/UC-f4cf47e1-d17f-4ff5-a47d-03894cb8a5dc/). **Course (original) Description**: Conviértete en un experto en Python con este curso que te enseña a programar desde cero. Con estas tecnologías podrás construir aplicaciones en el frontend y también en el backend. _By [Nicolas Schurmann](https://www.udemy.com/user/nicolas-schurmann/), Software Engineer._ -->

<!-- [ ![Python sin fronteras: HTML, CSS, Flask y MySQL](certificate/udemy-Python_sin_fronteras:_HTML,_CSS,_Flask,_y_MySQL.jpg){width=400px}\ ](https://www.udemy.com/certificate/UC-f4cf47e1-d17f-4ff5-a47d-03894cb8a5dc/) -->

### C

- [**Udemy (2021)**](https://www.udemy.com/course/c-programming-the-basics/): ["C: Programming the Basics"](https://www.udemy.com/course/c-programming-the-basics/). **Descripción del Curso**: Start with low-level C programming. _By [Frank Anemaet](https://www.udemy.com/user/frank-anemaet/), Software Engineering._

### Herramientas

- [**Udemy (2021)**](https://www.udemy.com/course/linux-tmux/): ["Linux Tmux"](https://www.udemy.com/course/linux-tmux/). **Descripción del Curso**: Linux terminal multiplexer. _By [Frank Anemaet](https://www.udemy.com/user/frank-anemaet/), Software Engineering._

## Publicaciones
- ![Argentina][icon-Argentina]\ [**CyTAL 2014**](http://www.edutecne.utn.edu.ar/cytal_frvm/CyTAL_2014/CyTAL_2014.pdf): "Ball bearings fault detection using FFT". **Original title**: _"Detección de fallas en rodamientos usando FFT"_.

## Proyectos
- "Software de gestión de tiempo y estadísticas para el análisis del desempeño de choferes de colectivos"


# Idiomas

## `◉◉◉◉◉◉` Español: Nativo

## `◉◉◉◉◉○` Inglés: Competencia profesional

## `◉◉◉◉○○` Alemán: B2.2


<!-- Make this section selectable on the Makefile -->
<!-- # Extra-Curricular -->
<!---->
<!-- ## Experiencia como Voluntario -->
<!---->
<!-- ### ![Germany][icon-Deutschland]\ TUI -->
<!-- - **Voluntario en Café estudiantil**: Trabajé como voluntario en el café ["bc-Studentencafé"](https://www.bc-studentencafe.de/en/homepage-english/) dentro del Campus de la TUI (Technische Universität Ilmenau). -->
<!---->
<!-- > Desde: 9/2014 hasta: 2/2015 (6 meses) -->
<!---->
<!-- ## Instrumentos musicales -->
<!-- - Actualmente estudiando piano. -->
<!-- - Varios años de estudio de guitarra clásica. -->

<!-- # Resources -->
<!-- ## Images / Icons -->
[icon-Argentina]: icon/arg.svg "flag-argentina" {height=15px}
[icon-Deutschland]: icon/ger.svg "flag-deutschland" {height=15px}
[icon-Spain]: icon/spain.svg "flag-spain" {height=15px}
[icon-LinkedIn]: icon/linkedin-logo.svg "logo-linkedin" {width=12px}
[icon-email]: icon/arroba.svg "icon-email" {width=12px}
[icon-University]: icon/mortarboard.svg "icon-university" {width=12px}
[icon-Research]: icon/research.svg "icon-research" {width=12px}

<!-- ## Links -->
[TUI]: https://www.tu-ilmenau.de/ "TUI"
[UTN]: https://www.utn.edu.ar/ "UTN"
[DAAD]: https://www.daad.de/en "DAAD"
[Fundación Fulgor]: https://www.linkedin.com/company/fundaci%C3%B3n-fulgor/ "Fundación Fulgor"
[My LinkedIn]: https://www.linkedin.com/in/matiasvico/?locale=en_US "My LinkedIn"
[mailto]: mailto:matias.daniel.vico@gmail.com?subject=[CV]%20e-mail%20contact "mailto"
