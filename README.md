# CV

My Curriculum Vitae

# ToDo
- [ ] Add a written description for the knowledge-level for languages.
- [ ] Add description for courses, publications and projects.
- [ ] Create a general, short presentation at the header of the CV. Add future development intentions.
- [ ] Objectives category? If so, propose a very narrow and specific one.
- [ ] References section and or secondary file (recommendation letters).
- [ ] Implement a conditional section (using Pandoc's plugin system or Make, or GPP, etc.) to be able to create a short, concise file and a longer, more precise one.
- [ ] References section
- [ ] Missing picture of the certificate of participation for the DDA course.
- [ ] 2024-10-10: Implement cover letters
- [ ] 2024-10-10: Automate xp calculation: "Up to this date, I count with more than $xp_years ..."
- [ ] 2024-10-10: Add `ECO` (`CECO` + `MECO`) experience
- [ ] 2024-10-10: Add Resposabilities per work place/role
- [ ] 2024-12-09: Finish the AWK course againt to obtain the missing certificate document to the 'certificates' folder

# Done
- [X] Insert emojis next to some categories like 'University' and country where it was made.
- [X] Add some sort of scale to show level of achievement/knowledge of a certain topic.
- [X] Find a way to show tags for keywords (skills equivalent of LinkedIn) in a non-invasive way.
- [X] Add description of main subjects of study in the different universities.
- [X] Convert everything that might be a link into it. For example to GECaM when there is a reference to it.
- [X] Look for 20xx/201x and replace with real year.

# Ideas
## Conditional generation
- [ ] Short (1, 2 pages) & Long (complete description)
- [ ] Differentiate Job searches
- [ ] Add/Remove skills based on the Job Search

# How to use
1. Execute: `source env/bin/activate` to enter on the `venv` for `python`
2. Run the desired `Makefile` rule, i.e: `make pdf`
