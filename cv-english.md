# Looking for
- Physical Design Engineering
- CAD flow/automation development
- Physical Verification

# Personal information
**Name**: Matías Daniel Vico

**Nationality**: Argentina ![Argentina][icon-Argentina]\

**Current Location**: Spain ![Spain][icon-Spain]\

## Contact information

![LinkedIn][icon-LinkedIn]\ **LinkedIn**: [www.linkedin.com/in/matiasvico][My LinkedIn]\

![email][icon-email]\ **e-mail**: [matias.daniel.vico@gmail.com][mailto]\


# Job experience
<!-- Personal Statement Essentials: As for format, you might dedicate one sentence to stating who you are, another to your background and accomplishments and a third to explain why you’re a good fit for the role. -->
Up to this date, I have accumulated more than {{years_since:3/2017}} years of experience in **Physical Design** _(`RTL` to `GDS`)_ and **CAD flow/automation development** _(Cadence and Synopsys toolsets)_.

Contributed to several successful tapeouts, developed in **well-stablished nodes**, as well as in **sub-micron, advanced nodes: 16, 7 and 5 nm**.

The main sectors I worked on are: **coherent-communications** ASICs destined to "Long Haul"/"Metro" and between/inside "Data Centers" communications. Also, on **power delivery systems** solutions.

## Current position
Sr. **Physical Design Engineer** at Monolithic Power Systems

> Since 7/2022

### Responsibilities 
- Digital block-level physical design, from `RTL` to `GDS`, for **highly physically constrained digital designs**
- Digital top-level physical design
- Custom Power Grid design and Power Analysis
- Custom Clock Tree Synthesis
- STA (Static Time Analysis)
- PV (Physical Verification)
- ECOs implementation
- CAD / Physical Design Workflow development for Cadence toolset

## Previous positions
Sr. **Physical Design Engineer** at Marvell Corporation

> From: 4/2021 to: 6/2022

Sr. **Physical Design Engineer** at Inphi Corporation

> From: 4/2018 to: 4/2021

Jr. **Physical Design Engineer** at Inphi Corporation

> From: 3/2017 to: 4/2018

### Responsibilities 
- Digital block-level physical design, from `RTL` to `GDS`, for **several multi-million-cells block instances**
- Digital chip-top-level physical design support
- Floorplanning for: block & chip-top level (digital-on-top projects)
- Custom Power Grid design and Power Analysis
- Custom Clock-Tree Synthesis for designs with multiple, global clocks (including its `SDC`'s definition) with frequencies as high as `1.2GHz`
- Focus on Low-Power design
- STA (Static Time Analysis)
- PV (Physical Verification)
- CAD / Physical Design Workflow development for Cadence & Synopsys toolsets


\newpage


# Education

## ![University][icon-University]\ University

### ![Argentina][icon-Argentina]\ UTN - FRVM
- **Master degree** in Electrical Engineering by the UTN - FRVM (Universidad Tecnológica Nacional - Facultad Regional Villa María). Villa María, Córdoba, Argentina.

- **Bachelor degree** in Electrical Engineering by the UTN - FRVM.

My education path focused on:

- ASIC manufacturing
- Analog and Digital IC design
- MEMS
- DFT / Verification
- Embedded systems / FPGA

#### Associated activities
- **GECaM** (Grupo de Estudio en Calidad en Mecatrónica, spanish for: Group of Study in Quality in Mechatronics): Research and development group for Mechanical and Electrical mixed system.

### ![Germany][icon-Deutschland]\ TUI
International academic exchange program. Collaboration between the [UTN][UTN]\ university and the German Academic Exchange Service ([DAAD][DAAD]\).

The exchange program was developed on the summer semester (_Sommerkurs_) in the [TUI (Technische Universität Ilmenau)][TUI].

The main objective of the scholarship was to take a usual semester of the career and to collaborate on a research program already in place on the mechanical and electrical faculty of the university.

Topic of the research project collaboration:

- **Low-Noise Amplifier development**: Collaboration in the development of a Low-Noise Amplifier for the 806 MHz band (band 20). Original title: _"Projekt MUSIK: 806 MHz (band 20) LNA entwurf"_

Some of the subjects I took on the semester:

- Nano-technology
- CMOS development

#### Associated activities

All associations made while on the exchange program were focused on interacting with people to learn and embrace the university and the German cultures:

- **Students Café volunteer**: Worked as a volunteer on the "bc-Studentencafé" on the TUI Campus.
- **Rugby**: Played Rugby with the TUI Rugby team.


## ![Scholarships][icon-research]\ Scholarships

### ![Argentina][icon-Argentina]\ CIN - Becas de Estímulo a las Vocaciones Científicas 2015

Implementation of: "Oscillation Based Test (OBT)" in modern reconfigurable analog circuits, a technique usually oriented towards embedded analog systems. Particularly working on re-configurable PSOC platforms from Cypress in development boards provided by the manufacturer. 

The validation of the proposed strategy was also developed by a fault-injection campaign on the implemented model.

Main topics: 

- `OBT`
- Analog design
- Continuos time and mixed-signal implementations

> Scholarship at the UTN - FRVM (Villa María, Córdoba, Argentina).
>
> From: 9/2015 to: 8/2016 (1 year).

### ![Argentina][icon-Argentina]\ GECaM (Grupo de Estudio en Calidad en Mecatrónica)
Possible applications for the FFT (Fast Fourier Transform) in preemptive analysis of vibrating systems.

Main topics:

- Mathematical methods needed to implement the FFT calculation
- Design of the software side for an already existent vibration-measuring device
- Analysis of the collected information to try to model fault behavior

> Scholarship at the UTN - FRVM (Villa María, Córdoba, Argentina).
>
> From: 8/2012 to: 7/2014 (2 years).



\newpage



# Main skills / Interests

## Electronic Engineering
- PD
- LEC
- STA
- PV
- Low-power
- CAD
- Spice
- ASIC
- SoC
- FPGA
- Genus
- Innovus
- Tempus
- Fusion Compiler / ICC2
- PrimeTime / PrimeECO
- Calibre
- StarRC

## Programming Languages / Scripting
- Tcl
- Python
- Bash
<!-- - System Verilog  Should learn it a little bit more/use it more to put it here-->
<!-- - [C](#c) this makes a internal link-->
- C
- Lua

## Version Control / Project management
- Git
- GitLab CI pipelines
- YouTrack

## OS
- Linux
- Windows
- MacOS

## Miscellaneous
- R&D
- Team-work
- [tmux / Byobu](https://www.udemy.com/course/linux-tmux/)
- [AWK](https://www.udemy.com/course/awk-tutorial/)


\newpage


# Accomplishments

## Courses

### Digital Design
- ![Argentina][icon-Argentina]\ [**Fundación Fulgor (2018)**][Fundación Fulgor]: "Advanced Digital Design course". **Original title**: _"Curso en Diseño Digital Avanzado (DDA)"_.

### Digital Verification
- [**Udemy (2021)**](https://www.udemy.com/course/soc-verification-systemverilog/): ["SoC Verification using System Verilog".](https://www.udemy.com/certificate/UC-04780e6a-6a86-4c24-9c71-de5e13e5a120/)

<!-- ![Udemy SoC Verification using System Verilog](certificate/udemy-SoC_Verification_using_System_Verilog.jpg){width=400px}\ -->

### Analog Design
- ![Argentina][icon-Argentina]\ [**Fundación Fulgor (2018)**](https://www.linkedin.com/company/fundaci%C3%B3n-fulgor/): "IC Analog Design". **Original title**: _"Diseño Analógico en Circuitos Integrados"_.
- ![Argentina][icon-Argentina]\ **EAMTA**: "Advanced Analog Design 2: 'Synopsys Custom Designer'".

### Micro-Controllers
- ![Argentina][icon-Argentina]\ **UTN - FRVM**: "Basic C PIC programming". **Original title**: _"Programación básica de PIC C"_.

### Python
- [**Kaggle (2020)**](https://www.kaggle.com/learn/python): ["Python"](https://www.kaggle.com/learn/certification/matasdanielvico/python). **Course Description**: Learn the most important language for data science. _By [Colin Morris](https://www.kaggle.com/colinmorris), Data Scientist_.

<!-- [ ![Kaggle "Python"](certificate/kaggle-python.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/python) -->

#### Python libraries

##### Pandas
- [**Kaggle (2021)**](https://www.kaggle.com/learn/pandas): ["Pandas"](https://www.kaggle.com/learn/certification/matasdanielvico/pandas). **Course Description**: Solve short hands-on challenges to perfect your data manipulation skills. _By [Aleksey Bilogur](https://www.kaggle.com/residentmario), Data Analyst._

<!-- [ ![Kaggle Pandas](certificate/kaggle-pandas.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/pandas) -->

##### `scikit-learn`
- [**Kaggle (2021)**](https://www.kaggle.com/learn/intro-to-machine-learning): ["Intro to Machine Learning"](https://www.kaggle.com/learn/certification/matasdanielvico/intro-to-machine-learning). **Course Description**: Learn the core ideas in machine learning, and build your first models. _By [Dan Becker](https://www.kaggle.com/dansbecker), Data Scientist._

<!-- [ ![Kaggle Intro to Machine Learning](certificate/kaggle-Intro_to_Machine_Learning.png){width=400px}\ ](https://www.kaggle.com/learn/certification/matasdanielvico/intro-to-machine-learning) -->

<!-- ##### Web & DB -->
<!-- - [**Udemy (2020)**](https://www.udemy.com/course/python-sin-fronteras-html-css-mysql/): ["Python sin fronteras: HTML, CSS, Flask y MySQL"](https://www.udemy.com/certificate/UC-f4cf47e1-d17f-4ff5-a47d-03894cb8a5dc/). **Course (original) Description**: Conviértete en un experto en Python con este curso que te enseña a programar desde cero. Con estas tecnologías podrás construir aplicaciones en el frontend y también en el backend. _By [Nicolas Schurmann](https://www.udemy.com/user/nicolas-schurmann/), Software Engineer._ -->

<!-- [ ![Python sin fronteras: HTML, CSS, Flask y MySQL](certificate/udemy-Python_sin_fronteras:_HTML,_CSS,_Flask,_y_MySQL.jpg){width=400px}\ ](https://www.udemy.com/certificate/UC-f4cf47e1-d17f-4ff5-a47d-03894cb8a5dc/) -->

### C

- [**Udemy (2021)**](https://www.udemy.com/course/c-programming-the-basics/): ["C: Programming the Basics"](https://www.udemy.com/course/c-programming-the-basics/). **Course Description**: Start with low-level C programming. _By [Frank Anemaet](https://www.udemy.com/user/frank-anemaet/), Software Engineering._

### Tools

- [**Udemy (2021)**](https://www.udemy.com/course/linux-tmux/): ["Linux Tmux"](https://www.udemy.com/course/linux-tmux/). **Course Description**: Linux terminal multiplexer. _By [Frank Anemaet](https://www.udemy.com/user/frank-anemaet/), Software Engineering._

- [**Udemy (2021)**](https://www.udemy.com/course/awk-tutorial/): ["AWK"](https://www.udemy.com/course/awk-tutorial/). **Course Description**: awk programming examples. _By [Frank Anemaet](https://www.udemy.com/user/frank-anemaet/), Software Engineering._

## Publications
- ![Argentina][icon-Argentina]\ [**CyTAL 2014**](http://www.edutecne.utn.edu.ar/cytal_frvm/CyTAL_2014/CyTAL_2014.pdf): "Ball bearings fault detection using FFT". **Original title**: _"Detección de fallas en rodamientos usando FFT"_.

## Projects
- "Time-management and metrics software for bus-drivers performance analysis". **Original title**: _"Software de gestión de tiempo y estadísticas para el análisis del desempeño de choferes de colectivos"_


# Languages

## `◉◉◉◉◉◉` Spanish: Native

## `◉◉◉◉◉○` English: Professional competence

## `◉◉◉◉○○` German: B2.2


<!-- Make this section selectable on the Makefile -->
<!-- # Extracurricular -->
<!---->
<!-- ## Volunteer experience -->
<!---->
<!-- ### ![Germany][icon-Deutschland]\ TUI -->
<!-- - **Students Café volunteer**: Worked as a volunteer on the ["bc-Studentencafé"](https://www.bc-studentencafe.de/en/homepage-english/) on the TUI (Technische Universität Ilmenau) Campus. -->
<!---->
<!-- > From: 9/2014 to: 2/2015 (6 months) -->
<!---->
<!-- ## Musical instruments -->
<!-- - Currently studying piano. -->
<!-- - Several years of classical guitar study. -->

<!-- # Resources -->
<!-- ## Images / Icons -->
[icon-Argentina]: icon/arg.svg "flag-argentina" {height=15px}
[icon-Deutschland]: icon/ger.svg "flag-deutschland" {height=15px}
[icon-Spain]: icon/spain.svg "flag-spain" {height=15px}
[icon-LinkedIn]: icon/linkedin-logo.svg "logo-linkedin" {width=12px}
[icon-email]: icon/arroba.svg "icon-email" {width=12px}
[icon-University]: icon/mortarboard.svg "icon-university" {width=12px}
[icon-Research]: icon/research.svg "icon-research" {width=12px}

<!-- ## Links -->
[TUI]: https://www.tu-ilmenau.de/ "TUI"
[UTN]: https://www.utn.edu.ar/ "UTN"
[DAAD]: https://www.daad.de/en "DAAD"
[Fundación Fulgor]: https://www.linkedin.com/company/fundaci%C3%B3n-fulgor/ "Fundación Fulgor"
[My LinkedIn]: https://www.linkedin.com/in/matiasvico/?locale=en_US "My LinkedIn"
[mailto]: mailto:matias.daniel.vico@gmail.com?subject=[CV]%20e-mail%20contact "mailto"
